module gitlab.com/inquizarus/rest

go 1.12

require (
	github.com/gorilla/mux v1.7.2
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
)
